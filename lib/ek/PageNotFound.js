export default 'Bulunamadı'

  .prop({
    name: 'Bulunamadı'
  })

  .layout([
    ['center', [
      'h1'.set('Bet Bulunamadı'),
      'p'.set('Aradığınız bet bulunamadı')
    ]]
  ])