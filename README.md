<img src="https://ilgilenio.github.io/Otag/img/otag.svg" alt="Otağ Logo" height="80">
  
### Otağ Çatı Çalışması

Süreğen bir JavaScript çatısı olan Otağ, **öncelikle ön uç şablonlama**daki **yalınlık sorununu çözer**. 

Kodlarınızı **yalın**laştırırırken dolaylı olarak **sunucu yükü ve harcamalarınızı azaltır**. 

**Soyutlamalar**, özel yapılar, **kolay Model/View Tanımı** ve **veri birleştirmeleri**yle, **büyük ölçekli** tasarılarınızda **kod bütünlüğünüzü** korur.

#### İlkeler
Otağ Çatı çalışması temel ilkeleri şunlardır:

* JavaScript prototip tabanlı olduğu için yöntemleri olabildiğince prototip olarak yazmak
* Ön yüz(Frontend) DOM ögelerinden oluştuğu için Otağ Bileşenlerini DOM ekseninde yazmak
* İstemci cihazını etkin kullanarak sunucu yükünü azaltırken kod kalitesi ve verimliliğe dikkat ederek aygıt pil ömrünü korumak
* Zengin bileşenleri tek biçimlilik ile yalınlaştırmak
* Bileşen(UI) ve Biçimler(Model) dahil **[Tamga](https://github.com/ilgilenio/Tamga)** yazımını kullanmak
* Yalınlık ve başarımı bir arada sağlamak
* En gerekli ve temel özellikleri barındırmak

#### Sürüm
Sürümleri ve etkinlikleri [takip edin 📆](https://ilgilenio.github.io/Otag/cizelge/)

1.3

[otag.js](https://ilgilenio.github.io/Otag/otag.1.3.js) (Geliştirme, Bol açıklamalı)

[otag.min.js](https://ilgilenio.github.io/Otag/otag.1.3.min.js) (**19.54KB**, _6.6kb GZIP_)


#### Belgelendirme
[Github Wiki](https://github.com/ilgilenio/Otag/wiki)

[GitLab Wiki](https://gitlab.com/ilgilenio/Otag/wikis) (Buraya taşınması sürüyor)

[OtagJS ORG](https://otagjs.org/#/belge)

#### Destek
GitLab üzerinde sorun(issue) [bildirebilirsiniz](https://gitlab.com/ilgilenio/Otag/issues).

#### Lisans
MIT Lisansı ile korunmaktadır.

#### Katılım sağlama
♥ Tasarının geliştirilmesi için bunları göz önünde bulundurularak katkı sağlayabilirsiniz.

#### İletişim
İşbirlikleri ve daha fazlası için <a href="mailto:bilgi@otagjs.org">bilgi@otagjs.org</a> bulunağına ileti gönderebilirsiniz.

[Tasarı gözlem yazıtımız](https://tree.taiga.io/project/ilgilenio-otag/kanban)'a gözatın
<a href="https://otagjs.org" title="Otağ'a ilerle">
    <img src="https://ilgilenio.github.io/Otag/img/otag.tengri.png" alt="Otağ, Tengri Dağı">
  </a>


